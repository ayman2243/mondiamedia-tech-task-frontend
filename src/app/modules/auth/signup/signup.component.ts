import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { AuthService } from '../../../services/auth/auth.service';
import {  User as UserRequestModel} from '../../../models/requests/user.model';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signupData = {firstName: null, lastName: null, email: null, password: null};
  signupForm: FormGroup;

  loading = false;
  invalidLogin = false;
  emailExistErr = false;

  constructor(private auth: AuthService, private router: Router) {
    this.signupForm = new FormGroup({
      'firstName': new FormControl(this.signupData.firstName, [Validators.required]),
      'lastName': new FormControl(this.signupData.lastName, [Validators.required]),
      'email': new FormControl(this.signupData.email, [Validators.required, Validators.email]),
      'password': new FormControl(this.signupData.password, [Validators.required])
    });
  }

  signup(){
    this.invalidLogin = false;
    this.loading = true;
    this.emailExistErr = false;
    let user = new UserRequestModel(this.signupData);
    this.auth.signup(user).subscribe(
      (user) =>{
        this.loading = false;
        this.router.navigate(['/auth', 'login']);
      },
      (err) => {
        console.log(err);
        
        if(err.status === 500 && JSON.parse(err['_body'])["message"] === "Email already exists!" ){
          this.emailExistErr = true;
        }
        this.loading = false;
        this.invalidLogin = true;
      }
    )
  }

  ngOnInit() {
  }

}
