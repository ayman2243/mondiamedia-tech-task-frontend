import { Component, OnInit, Input } from '@angular/core';
import { Router } from "@angular/router";
import { DataService } from '../../../services/shared/data.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {
  
  authUser: boolean;
  user:Object;

  constructor(private router: Router, private shared: DataService) { 
    localStorage.removeItem('userId');
    localStorage.removeItem('token');
    this.router.navigate(['/']);
    this.shared.authUser.next(false);
    this.shared.user.next(null);
  }

  ngOnInit() {
  }

}
