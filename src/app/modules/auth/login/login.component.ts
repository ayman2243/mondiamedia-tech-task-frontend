import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { AuthService } from '../../../services/auth/auth.service';
import {  User as UserRequestModel} from '../../../models/requests/user.model';
import {  User as UserResponseModel} from '../../../models/responses/user.model';
import { DataService } from '../../../services/shared/data.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  loginData = {email: null, password: null};
  loginForm: FormGroup;

  loading = false;
  invalidLogin = false;


  constructor(private auth: AuthService, private router: Router, private shared: DataService) { 
    this.loginForm = new FormGroup({
      'email': new FormControl(this.loginData.email, [Validators.required, Validators.email]),
      'password': new FormControl(this.loginData.password, [Validators.required])
    });
  }

  login(){
    this.invalidLogin = false;
    this.loading = true;
    let user = new UserRequestModel(this.loginData);
    this.auth.login(user).subscribe(
      (user) =>{
        this.loading = false;
        localStorage.setItem('token', user.token);
        localStorage.setItem('userId', user.userId);
        this.auth.me(localStorage.getItem('token')).subscribe(
          (me) => {
            this.shared.authUser.next(true);
            this.shared.user.next(me);
          }
        );
        this.router.navigate(['/home']);
        
      },
      (err) => {
        this.loading = false;
        this.invalidLogin = true;
      }
    )
  }

  ngOnInit() {
  }

}
