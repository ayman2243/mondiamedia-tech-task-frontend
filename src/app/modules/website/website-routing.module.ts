import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: "",
    loadChildren: "./landing/landing.module#LandingModule"
  },
  {
    path: "category",
    loadChildren: "./categories/categories.module#CategoriesModule"
  },
  {
    path: "users",
    loadChildren: "./users/users.module#UsersModule"
  },
  {
    path: "videos",
    loadChildren: "./videos/videos.module#VideosModule"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WebsiteRoutingModule { }
