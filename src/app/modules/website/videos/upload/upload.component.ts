import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { VideoService } from "./../../../../services/video/video.service";

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  uploadData = {category: null, title: null, description: null, video: null };
  uploadForm: FormGroup;

  loading = false;
  invalidLogin = false;

  selectedFiles: FileList


  constructor(private videoService: VideoService, private router: Router) { 
    this.uploadForm = new FormGroup({
      'category': new FormControl(this.uploadData.category, [Validators.required]),
      'title': new FormControl(this.uploadData.title, [Validators.required]),
      'description': new FormControl(this.uploadData.description, [Validators.required]),
      'video': new FormControl(this.uploadData.video, [Validators.required])
    });
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }


  upload(){
    this.invalidLogin = false;
    this.loading = true;

    this.uploadData.video = this.selectedFiles.item(0)
    let formData = new FormData();
    formData.append('video', this.uploadData.video);
    formData.append('title', this.uploadData.title);
    formData.append('description', this.uploadData.description);
    formData.append('category', this.uploadData.category);

    this.videoService
    .uploadVideo(
        localStorage.getItem('token'), 
        formData)
    .subscribe(
      (video) => {
        this.loading = false;
        this.router.navigate(['/videos', video.videoId]);
      },
      (err) => {
        this.loading = false;
        this.invalidLogin = true;
      }
    );
  }

  ngOnInit() {
  }

}
