import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { MatSnackBar } from '@angular/material';
import { VideoService } from '../../../../services/video/video.service';
import { UserService } from '../../../../services/user/user.service';

@Component({
  selector: 'app-watch',
  templateUrl: './watch.component.html',
  styleUrls: ['./watch.component.scss']
})
export class WatchComponent implements OnInit {

  userInfo;
  videoId;
  video;
  authUserId = localStorage.getItem('userId') || null;

  constructor(
    private snackBar: MatSnackBar,
    private router: Router, 
    private activatedRoute:ActivatedRoute,
    private videoService: VideoService,
    private userService: UserService
    ) {  
      this.videoId = this.activatedRoute.snapshot.params['videoId'];
      this.getVideoInfo(this.videoId);
    }

  getUserInfo(userId){
    this.userService.getUserByUserId(userId)
      .subscribe(
        (user) => {
          this.userInfo = user;
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getVideoInfo(videoId){
    this.videoService.getVideoByVideoId(this.videoId)
      .subscribe(
        (video) => {
          this.video = video;
          this.getUserInfo(this.video.userId);
        },
        (err) => {
          if(err.status === 404){
            alert('Opps, video not found!');
            this.router.navigate(['/']);
          }
        }
      );
  }

  deleteVideo(){
    this.videoService.deleteVideo(
      localStorage.getItem('token'), 
      this.videoId
      ).subscribe(
        (ok) => {
          this.router.navigate(['/']);
        },
        (err) =>{
          console.log(err);
        }
      )
  }

  showAlert(){
    this.snackBar
    .open('This is only a demo, \n can\'t paly the video', 'OK', {
      duration: 100000,
    });
  }

  ngOnInit() {
  }

}
