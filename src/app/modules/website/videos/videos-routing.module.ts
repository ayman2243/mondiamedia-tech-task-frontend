import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../../../guards/auth/auth.guard';

import { AllComponent } from './all/all.component';
import { UploadComponent } from './upload/upload.component';
import { ChannelComponent } from './channel/channel.component';
import { WatchComponent } from './watch/watch.component';
import { EditComponent } from './edit/edit.component';


const routes: Routes = [
  { path: '', redirectTo: 'all', pathMatch: 'full' },
  { path: 'all', component: AllComponent },
  { path: 'upload', component: UploadComponent, canActivate: [AuthGuard] },
  { path: 'channel/:userId', component: ChannelComponent },
  { path: 'edit/:videoId', component: EditComponent, canActivate: [AuthGuard] },
  { path: ':videoId', component: WatchComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideosRoutingModule { }
