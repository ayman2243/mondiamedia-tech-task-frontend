import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { VideoService } from '../../../../services/video/video.service';
import { Video as VideoRequestModel } from '../../../../models/requests/video.model';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  videoId;
  videoDownloadUrl;

  editVideoData = { category: null, title: null, description: null };
  editVideoForm: FormGroup;

  loading = false;
  invalidLogin = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private videoService: VideoService
  ) {
    this.videoId = this.activatedRoute.snapshot.params['videoId'];
    this.getVideoInfo(this.videoId);
  
    this.editVideoForm = new FormGroup({
      'category': new FormControl(this.editVideoData.category, [Validators.required]),
      'title': new FormControl(this.editVideoData.title, [Validators.required]),
      'description': new FormControl(this.editVideoData.description, [Validators.required]),
    });
  }


  getVideoInfo(videoId) {
    this.videoService.getVideoByVideoId(this.videoId)
      .subscribe(
        (video) => {
          this.editVideoData.title = video.title;
          this.editVideoData.category = video.category;
          this.editVideoData.description = video.description;
          this.videoDownloadUrl = video.videoDownloadUrl;
        },
        (err) => {
          if (err.status === 404) {
            alert('Opps, video not found!');
            this.router.navigate(['/']);
          }
        }
      );
  }

  editVideo(){
    this.videoService
      .editVideo(
        localStorage.getItem('token'),
        this.videoId,
        new VideoRequestModel(this.editVideoData)
        )
      .subscribe(
        (video) =>{
          this.router.navigate(['/videos', video.videoId]);
        },
        err => {
          console.log(err);
        }
      );
  }

  ngOnInit() {
  }

}
