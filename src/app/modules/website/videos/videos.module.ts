import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { VideosRoutingModule } from './videos-routing.module';
import { AllComponent } from './all/all.component';
import { UploadComponent } from './upload/upload.component';
import { ChannelComponent } from './channel/channel.component';
import { WatchComponent } from './watch/watch.component';
import { EditComponent } from './edit/edit.component';

import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from "@angular/material/icon";
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  imports: [
    CommonModule,
    VideosRoutingModule,
    MatDividerModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatSnackBarModule,
    MatSelectModule,
    FormsModule, 
    ReactiveFormsModule
  ],
  declarations: [AllComponent, UploadComponent, ChannelComponent, WatchComponent, EditComponent]
})
export class VideosModule { }
