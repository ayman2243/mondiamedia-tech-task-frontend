import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { VideoService } from '../../../../services/video/video.service';
import { UserService } from '../../../../services/user/user.service';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.scss']
})
export class ChannelComponent implements OnInit {

  userId;
  videos;
  user;
  constructor(
    private router: Router, 
    private activatedRoute:ActivatedRoute,
    private videoService: VideoService,
    private userService: UserService
  ) { 
    this.userId = this.activatedRoute.snapshot.params['userId'];
    this.getUserVideos();
  }

  getUserInfo(){
    this.userService.getUserByUserId(this.userId)
    .subscribe(
      (user) =>{
        this.user = user;
      },
      (err) =>{
        console.log(err);
      }
    )
  }

  getUserVideos(){
    this.videoService.getUserChannel(this.userId)
    .subscribe(
      (videos) => {
        this.videos = videos;
        this.getUserInfo();
      },
      (err) => {
        console.log(err);
      }
    )
  }

  ngOnInit() {
  }

}
