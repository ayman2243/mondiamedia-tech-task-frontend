import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { VideoService } from '../../../../services/video/video.service';
import { UserService } from '../../../../services/user/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  userId;
  user;

  constructor(
    private router: Router, 
    private activatedRoute:ActivatedRoute,
    private userService: UserService
  ) { 
    this.userId = this.activatedRoute.snapshot.params['userId'];
    this.getUserInfo();
  }

  getUserInfo(){
    this.userService.getUserByUserId(this.userId)
    .subscribe(
      (user) =>{
        this.user = user;
      },
      (err) =>{
        console.log(err);
      }
    )
  }

  ngOnInit() {
  }

}
