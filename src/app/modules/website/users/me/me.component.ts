import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../services/user/user.service';

@Component({
  selector: 'app-me',
  templateUrl: './me.component.html',
  styleUrls: ['./me.component.scss']
})
export class MeComponent implements OnInit {

  userId;
  user;

  constructor(
    private userService: UserService
  ) { 
    this.userId = localStorage.getItem('userId');
    this.getUserInfo();
  }

  getUserInfo(){
    this.userService.getUserByUserId(this.userId)
    .subscribe(
      (user) =>{
        this.user = user;
      },
      (err) =>{
        console.log(err);
      }
    )
  }

  ngOnInit() {
  }

}
