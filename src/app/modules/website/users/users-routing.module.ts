import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../../../guards/auth/auth.guard';

import { ProfileComponent } from './profile/profile.component';
import { MeComponent } from './me/me.component';

const routes: Routes = [
  { path: '', redirectTo: 'me', pathMatch: 'full' },
  { path: 'me', component: MeComponent, canActivate: [AuthGuard] },
  { path: ':userId', component: ProfileComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
