import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebsiteRoutingModule } from './website-routing.module';

@NgModule({
  imports: [
    CommonModule,
    WebsiteRoutingModule
  ],
  declarations: []
})
export class WebsiteModule { }
