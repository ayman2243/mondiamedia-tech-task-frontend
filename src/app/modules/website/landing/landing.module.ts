import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { HomeComponent } from './home/home.component';

import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from "@angular/material/icon";

@NgModule({
  imports: [
    CommonModule,
    LandingRoutingModule,
    MatDividerModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule
  ],
  declarations: [HomeComponent]
})
export class LandingModule { }
