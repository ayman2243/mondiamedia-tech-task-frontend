import { Component, OnInit } from '@angular/core';
import { VideoService } from '../../../../services/video/video.service';
import { UserService } from '../../../../services/user/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  latestUsers: Array<any>;
  latestVideos: Array<any>;

  constructor(
    private videoService: VideoService,
    private userService: UserService
  ) {
    this.getLatestVideos();
    this.getLatestUsersInTheSystem();
  }


  ngOnInit() {
  }

  getLatestUsersInTheSystem() {
    this.userService.getLatestUsers()
      .subscribe(
        (users) => {
          this.latestUsers = users;
        },
        (err) => {
          console.log(err);
        }
      )
  }

  getLatestVideos() {
    this.videoService.getAllVideos(localStorage.getItem('token')).subscribe(
      (videos: Array<any>) => {
        this.latestVideos = videos;
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
