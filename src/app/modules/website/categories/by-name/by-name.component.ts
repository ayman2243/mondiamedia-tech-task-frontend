import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { VideoService } from '../../../../services/video/video.service';

@Component({
  selector: 'app-by-name',
  templateUrl: './by-name.component.html',
  styleUrls: ['./by-name.component.scss']
})
export class ByNameComponent implements OnInit {

  videos: Array<any>;
  categoryName: String;
  constructor( 
    private router: Router, 
    private activatedRoute: ActivatedRoute,
    private videoService: VideoService) {
    this.categoryName = this.activatedRoute.snapshot.params['categoryName'];
    this.getVideos();
  }

  getVideos(){
    this.videoService.getVideosByCategory(this.categoryName)
      .subscribe(
        (videos: Array<any>) => {
          this.videos =videos;
        },
        (err) => {
          console.log(err);
        }
      )
  }

  ngOnInit() {
  }

}
