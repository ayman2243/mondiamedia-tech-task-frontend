import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { IAppConfig } from './app-config.model';

@Injectable()
export class AppConfig {

    static settings: IAppConfig;
    private configObject = {
        dev:{
            "env": {
            "name": "develop"
            },
            "apiServer": {
                "url": "http://localhost:8080"
            }
        },
        prod:{
            "env": {
            "name": "production"
            },
            "apiServer": {
                "url": "https://mondiamedia-tech-task.herokuapp.com"
            }
        }
    }
    
    constructor() {}

    load() {
        AppConfig.settings = <IAppConfig>this.configObject[environment.name];
        return true;
    }
}