export interface IAppConfig {
    env: {
        name: String;
    };
    apiServer: {
        url: String;
    };
}