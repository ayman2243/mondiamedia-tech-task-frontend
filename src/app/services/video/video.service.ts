import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';
import { AppConfig } from '../../config/app.config';
import { VideoServiceInterface } from '../interfaces/video.interface';
import { Video as VideoRequestModel } from "./../../models/requests/video.model";
import { Video as VideoResponseModel } from "./../../models/responses/video.model";
import { User as UserRequestModel } from './../../models/requests/user.model';

@Injectable({
  providedIn: 'root'
})
export class VideoService implements VideoServiceInterface {

  constructor(private http: Http) { }

  uploadVideo(token: String, video: any): Observable<VideoResponseModel>{
    let headers = new Headers;
    headers.append("Authorization", `${token}`);
    let options = new RequestOptions({ headers: headers });
    return this.http.post(
      `${AppConfig.settings.apiServer.url}/videos`,
      video,
      options).pipe(
        map((res: Response) => new VideoResponseModel(res.json()))
      );
  }

  getAllVideos(token: String): Observable<VideoResponseModel[]>{
    let headers = new Headers;
    headers.append("Authorization", `${token}`);
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers });
    return this.http.get(
      `${AppConfig.settings.apiServer.url}/videos?page=0&limit=8`,
      options).pipe(
        map((res: Response) => {
          let videos = [];
          res.json().forEach(video => {
            videos.push(new VideoResponseModel(video))
          });
          return videos;
        })
      );
  }

  getVideoByVideoId(videoId: String): Observable<VideoResponseModel>{
    let headers = new Headers;
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers });
    return this.http.get(
      `${AppConfig.settings.apiServer.url}/videos/${videoId}`,
      options).pipe(
        map((res: Response) => new VideoResponseModel(res.json()))
      );
  }

  editVideo(token: String, videoId: String,  video: VideoRequestModel): Observable<VideoResponseModel>{
    let headers = new Headers;
    headers.append("Authorization", `${token}`);
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers });
    return this.http.put(
      `${AppConfig.settings.apiServer.url}/videos/${videoId}`,
      video,
      options).pipe(
        map((res: Response) => new VideoResponseModel(res.json()))
      );
  }

  getVideosByCategory(categoryName: String): Observable<VideoResponseModel[]>{
    let headers = new Headers;
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers });
    return this.http.get(
      `${AppConfig.settings.apiServer.url}/videos/category/${categoryName}`,
      options).pipe(
        map((res: Response) => {
          let videos = [];
          res.json().forEach(video => {
            videos.push(new VideoResponseModel(video))
          });
          return videos;
        })
      );
  }

  getUserChannel(userId: String): Observable<VideoResponseModel[]>{
    let headers = new Headers;
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers });
    return this.http.get(
      `${AppConfig.settings.apiServer.url}/videos/channel/${userId}`,
      options).pipe(
        map((res: Response) => {
          let videos = [];
          res.json().forEach(video => {
            videos.push(new VideoResponseModel(video))
          });
          return videos;
        })
      );
  }

  deleteVideo(token: String, videoId: String): Observable<any>{
    let headers = new Headers;
    headers.append("Authorization", `${token}`);
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers });
    return this.http.delete(
      `${AppConfig.settings.apiServer.url}/videos/${videoId}`,
      options).pipe(
        map((res: Response) => res.json())
      );
  }
}
