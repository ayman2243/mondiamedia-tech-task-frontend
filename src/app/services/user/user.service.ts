/*   
* 
* Designed & Developed By Ayman Aljohary
* Email: ayman2243@gmail.com
* Phone: +201201035118
* Linkedin: https://www.linkedin.com/in/ayman-algohary-17b86567/
*
*/

import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';
import { AppConfig } from '../../config/app.config';
import { UserServiceInterface } from '../interfaces/user.interface';
import { User as UserResponseModel } from './../../models/responses/user.model';
import { User as UserRequestModel } from './../../models/requests/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService implements  UserServiceInterface{

  constructor(private http: Http) { }

  getLatestUsers(): Observable<UserResponseModel[]>{
    let headers = new Headers;
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers });
    return this.http.get(
      `${AppConfig.settings.apiServer.url}/users`,
      options).pipe(
        map((res: Response) =>{
          let users = [];
          res.json().forEach((user,index) => {
            users.push(new UserResponseModel(user))
          });
          return users;
        })
      );
  }

  getUserByUserId(userId: String): Observable<UserResponseModel>{
    let headers = new Headers;
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers });
    return this.http.get(
      `${AppConfig.settings.apiServer.url}/users/${userId}`,
      options).pipe(
        map((res: Response) => new UserResponseModel(res.json()))
      );
  }
  
  updateProfile(token: String, user: UserRequestModel): Observable<UserResponseModel>{
    let headers = new Headers;
    headers.append("Authorization", `${token}`);
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers });
    return this.http.put(
      `${AppConfig.settings.apiServer.url}/users/${user.userId}`,
      user,
      options).pipe(
        map((res: Response) => new UserResponseModel(res.json()))
      );
  }

  deleteProfile(token: String, user: UserRequestModel): Observable<any>{
    let headers = new Headers;
    headers.append("Authorization", `${token}`);
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers });
    return this.http.delete(
      `${AppConfig.settings.apiServer.url}/users/${user.userId}`,
      options).pipe(
        map((res: Response) => res.json())
      );
  }
  
}
