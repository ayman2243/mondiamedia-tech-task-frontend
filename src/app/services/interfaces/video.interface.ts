/*   
* 
* Designed & Developed By Ayman Aljohary
* Email: ayman2243@gmail.com
* Phone: +201201035118
* Linkedin: https://www.linkedin.com/in/ayman-algohary-17b86567/
*
*/

import { Observable } from "rxjs";
import { Video as VideoRequestModel } from "./../../models/requests/video.model";
import { Video as VideoResponseModel } from "./../../models/responses/video.model";
import { User as UserRequestModel } from './../../models/requests/user.model';

export interface VideoServiceInterface{

    uploadVideo(token: String, video: any): Observable<VideoResponseModel>;
    
    getAllVideos(token: String): Observable<VideoResponseModel[]>;
    
    getVideoByVideoId(videoId: String): Observable<VideoResponseModel>;
    
    editVideo(token: String, videoId: String, video: VideoRequestModel): Observable<VideoResponseModel>;
    
    getVideosByCategory(categoryName: String): Observable<VideoResponseModel[]>;
    
    getUserChannel(userId: String): Observable<VideoResponseModel[]>;

    deleteVideo(token: String, videoId: String): Observable<any>;

}