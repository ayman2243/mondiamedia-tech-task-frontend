/*   
* 
* Designed & Developed By Ayman Aljohary
* Email: ayman2243@gmail.com
* Phone: +201201035118
* Linkedin: https://www.linkedin.com/in/ayman-algohary-17b86567/
*
*/

import { Observable } from "rxjs";
import { User as UserResponseModel } from './../../models/responses/user.model';
import { User as UserRequestModel } from './../../models/requests/user.model';

export interface UserServiceInterface{
    getLatestUsers(): Observable<UserResponseModel[]>;
    getUserByUserId(user: String): Observable<UserResponseModel>;
    updateProfile(token: String, user: UserRequestModel): Observable<UserResponseModel>;
    deleteProfile(token: String, user: UserRequestModel): void;
}