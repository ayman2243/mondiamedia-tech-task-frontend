/*   
* 
* Designed & Developed By Ayman Aljohary
* Email: ayman2243@gmail.com
* Phone: +201201035118
* Linkedin: https://www.linkedin.com/in/ayman-algohary-17b86567/
*
*/

import { Observable } from "rxjs";
import { User as UserRequestModel } from './../../models/requests/user.model';
import { User as UserResponseModel } from './../../models/responses/user.model';

export interface AuthServiceInterface{
    signup(user: UserRequestModel): Observable<UserResponseModel>;
    login(user: UserRequestModel): Observable<any>;
    logout():void;
}