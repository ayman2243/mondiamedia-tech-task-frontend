/*   
* 
* Designed & Developed By Ayman Aljohary
* Email: ayman2243@gmail.com
* Phone: +201201035118
* Linkedin: https://www.linkedin.com/in/ayman-algohary-17b86567/
*
*/

import { AuthServiceInterface } from '../interfaces/auth.interface';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';
import { AppConfig } from '../../config/app.config';
import { User as UserResponseModel } from './../../models/responses/user.model';
import { User as UserRequestModel } from './../../models/requests/user.model';


@Injectable({
  providedIn: 'root'
})
export class AuthService implements AuthServiceInterface {

  constructor(private http: Http) { }

  signup(user: UserRequestModel): Observable<UserResponseModel>{
    let headers = new Headers;
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers });
    return this.http.post(
      `${AppConfig.settings.apiServer.url}/users`,
      user,
      options).pipe(
        map((res: Response) => {
          return new UserResponseModel(res.json());
        })
      );
  }

  login(user: UserRequestModel): Observable<any>{
    return this.http.post(
      `${AppConfig.settings.apiServer.url}/users/login`,
      user).pipe(
        map((res: Response) => {
          return { userId: res.json().userId, token: res.json().token };
        })
      );
  }

  async isAuthenticated() {
    // check User
    if (!localStorage.getItem('token') || !localStorage.getItem('userId'))
      throw false;
    var user = await this.me(localStorage.getItem('token')).toPromise();
    if (user)
      return true;
    throw false;
  }


  me(token: String): Observable<UserResponseModel> {
    let headers = new Headers;
    headers.append("Authorization", `${token}`);
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers });
    return this.http.get(
      `${AppConfig.settings.apiServer.url}/users/me`,
      options).pipe(
        map((res: Response) => new UserResponseModel(res.json()))
      );
  }

  logout():void{}

}
