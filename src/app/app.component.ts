import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth/auth.service';
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from './store/store';
import { DataService } from './services/shared/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent implements OnInit {
  @select('users') usersInStore;
  user: Object;
  authUser: boolean;

  constructor(private auth: AuthService, private ngRedux: NgRedux<IAppState>, private shared: DataService) {
    this.shared.authUser.subscribe(value => {
      this.authUser = value;
    });
    this.shared.user.subscribe(value => {
      this.user = value;
    });

    this.usersInStore.subscribe((users) =>{
      console.log(users);
    });
  } 

  getUserInfo(){
    this.auth.me(localStorage.getItem('token')).subscribe(
      (me) => {
        this.user = me;
        this.shared.authUser.next(true);
        this.shared.user.next(me);
      },
      err =>{
        this.shared.authUser.next(false);
        this.shared.user.next(null);
      }
    );
  }

  ngOnInit(){
    if(localStorage.getItem('token')){
      this.getUserInfo();
    }else{
      this.shared.authUser.next(false);
    }
  }
}
