export const ADD_USER = 'ADD_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const DELETE_USER = 'DELETE_USER';
export const REMOVE_ALL_USERS = 'REMOVE_ALL_USERS';