
export class Video {
    video: any;
    videoId: String;
    category: any;
    title: any;
    description: any;
    videoDownloadUrl: String;

    constructor(input: any) {
        Object.assign(this, input);
        return this;
    }
}