export class Video {
    userId: String;
    videoId: String;
    category: String;
    title: String;
    description: String;
    videoDownloadUrl: String;

    constructor(input: any) {
        Object.assign(this, input);
        return this;
    }
}