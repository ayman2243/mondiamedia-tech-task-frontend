export class User {
    userId: String;
    firstName: String;
    lastName: String;
    email: String;

    constructor(input: any) {
        Object.assign(this, input);
        return this;
    }
}
